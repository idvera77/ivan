
package poo;


public class Vaca {
    String nombre;
    int numero_serie;
    float peso;
    int edad;
    String funcion;
    float [] litrosOrdenados;
    
    /**
     * 
     * @param n nombre
     * @param ns numero de serie
     * @param p peso
     * @param e edad
     * @param f funcion
     */
    public Vaca(String n, int ns, float p, int e, String f){
        this.nombre=n;
        this.numero_serie=ns;
        this.peso=p;
        this.edad=e;
        this.funcion=f;
        this.litrosOrdenados=new float[15];
    }
    
    public Vaca(String n, int ns, float p, int e){
        this.nombre=n;
        this.numero_serie=ns;
        this.peso=p;
        this.edad=e;
        this.asignaFuncion();
        this.litrosOrdenados=new float[15];
    }
    
    public void asignaFuncion(){
        if(this.peso>200){
            this.funcion=Constantes.carniceria;
        }else{
            this.funcion=Constantes.ordeño;
        }
    } 
    
     public String datosVaca(){
        String salida="Nombre: "+this.nombre+
                "\nIdentificador: "+this.numero_serie+
                "\nFuncion: "+this.funcion+
                "\nAños: "+this.edad+
                "\nPeso: "+this.peso+
                " Kg\n";
                for(int i=0;i<litrosOrdenados.length;i++){
                   salida+=litrosOrdenados[i]+" ";
                }
                return salida;
    }      
}
