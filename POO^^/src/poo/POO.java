
package poo;


import java.util.Scanner;


public class POO {
    
    
    public static void main(String[] args) {
        
        //Datos primera persona
        Persona p1=new Persona("Ivan","Diaz",30,1.71f,"gerencia");
        
        //Datos primera persona         
        Persona p2=new Persona("Ana","Molina",29,1.60f,"ordeño");

        //Datos primera vaca
        Vaca v1=new Vaca("Tolon",007,150,5);
   
        //Datos segunda vaca
        Vaca v2=new Vaca("Mu",212,350,10);

        Vaca []vac={v1,v2};

        //Mostrando datos de p1 y p2
        System.out.println(p1.imprimePersona());
        System.out.println("");
        System.out.println(p2.imprimePersona());
        System.out.println("----------------------\n");
        
        //Funcion esMayorDe (edad)
        System.out.println("Tiene "+p1.nombre+" mas de 29 años?\n"+p1.esMayorDe(29));
        System.out.println("");
        System.out.println("Tiene "+p2.nombre+" mas de 29 años?\n"+p2.esMayorDe(29));
        System.out.println("----------------------\n");
        
        //Funcion cuantoGana(muestra cadena)
        System.out.println(p1.cuantoGana());
        System.out.println(p2.cuantoGana());
        System.out.println("----------------------\n");

        //Funcion proporcion, muestra cuantas veces gana esa persona el sueldo de la otra.
        System.out.println(p1.proporcionSueldo(p2));       
        System.out.println(p2.proporcionSueldo(p1));
        System.out.println("----------------------\n");

        System.out.println(p1.ajustaSueldoA(p2));
        System.out.println(p2.ajustaSueldoA(p1));
        System.out.println("----------------------\n");
        
        //Añade funcion a las vacas dependiendo del peso asignado.
        v1.asignaFuncion();
        v2.asignaFuncion();
        System.out.println("----------------------\n");
        System.out.println(v1.datosVaca());
        System.out.println("");
        System.out.println(v2.datosVaca());
        
        //Guardo y muestro vacas.
        System.out.println("----------------------\n");
        p1.quedarmeVacas(vac);
        p1.tengovacas();
        System.out.println("");
        p2.quedarmeVacas(vac);
        p2.tengovacas();
        System.out.println("");
        
        System.out.println("");
        System.out.println(p1.imprimetodo());
        System.out.println(p2.imprimetodo());
        
        /*
        //Guardar datos nuevas personas
        System.out.println("Indica el numero de personas a introducir en el sistema");
        Scanner sc=new Scanner(System.in);
        int numero=sc.nextInt();
        sc.nextLine();
        
        Persona[]ap=new Persona [numero];
        for(int i=0;i<ap.length;i++){
            ap[i]=new Persona();
            System.out.println("¿Nombre?");
            ap[i].nombre=sc.nextLine();
            System.out.println("¿Apellido");
            ap[i].apellido=sc.nextLine();
            System.out.println("¿Edad?");
            ap[i].edad=Integer.parseInt(sc.nextInt()); 
            System.out.println("¿Altura?");
            ap[i].altura=sc.nextFloat();
            System.out.println("¿puesto?");
            ap[i].puesto=sc.nextLine();
            System.out.println("¿sueldo?");
            ap[i].sueldo=sc.nextFloat();
            System.out.println("");
        
        }
        */
    }          
}

/*
-Añadir a la clase Persona un String "puesto". Usarlo solo para valores "ordeño","carniceria" o "gerencia".
-Añadir a la clase Persona un Float "sueldo", que expresará el sueldo en euros.
-añadir un método cuantoGana a la clase Persona, que devuelva el String "Gana"+sueldo+" €"
-añadir un método proporcionSueldo a la clase Persona, que reciba una persona por argumentos, y devuelva un float con cuántas veces gana esa persona el sueldo de this.
-Añadir un método ajustaSueldoA, que reciba un objeto Persona, y asigne el sueldo de this, poniéndolo en proporción al del objeto que recibe por parámetro. 
Un/a ordeñador/a debería ganar un 25% más que alguien de carnicería, y un/a gerente un 50% más que alguien de ordeño.
-Crear la clase Vaca, con Strings para nombre, número de serie y función ("ordeño" o "carniceria"), un Float peso, un int edad, y un array de float con litros ordeñados en el día.
-Crear un método asignaFunción a una vaca, de forma que si pesa más de 200kg, se asigne a carnicería, y si pesa menos, a ordeño.
-Añadir a la clase persona un Array de Vacas
-Crear en la clase persona un método quedarmeVacas(Vaca[] vac), que copie en el array interno a las vacas que corresponden a su puesto de trabajo.
*/