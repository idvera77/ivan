
package poo;


public class Persona {
    String nombre;
    String apellido;
    int edad;
    float altura;
    String puesto;
    float sueldo;
    Vaca[]vacas;
    
    public Persona(String n, String ap, int e, float alt, String p){
        this.nombre=n;
        this.apellido=ap;
        this.edad=e;
        this.altura=alt;
        this.puesto=p;
        if(this.puesto.equals(Constantes.gerencia)){
            this.sueldo=2000;
        }else if(this.equals(Constantes.ordeño)){
            this.sueldo=1000;
        }else{
            this.sueldo=750;
        }
        this.vacas=new Vaca[3];
    }
    
    //Funcion para imprimir todos los valores de Persona
    public String imprimePersona(){
        return "Nombre: "+this.nombre+
                "\nApellido: "+this.apellido+
                "\nEdad: "+this.edad+"\nAltura "
                +this.altura+" cm\nPuesto: "
                +this.puesto+"\nSueldo: "
                +this.sueldo+"€";
    }
    
    public String imprimetodo(){
        String salida= "Nombre: "+this.nombre+
                "\nApellido: "+this.apellido+
                "\nEdad: "+this.edad+"\nAltura "
                +this.altura+" cm\nPuesto: "
                +this.puesto+"\nSueldo: "
                +this.sueldo+"€\n";
        for(int i=0;i<vacas.length;i++){
            if(vacas[i]!=null){
                salida+=vacas[i].nombre+"";
            }
        }
       return salida;
    }
    
    /**
     * Dice si la persona que llama a la funcion es mayor de una cierta edad
     * @param int edad la edad con la que vamos a comprar a la persona
     * @return true si la persona es mayor, false si es menor o de igual edad
     */
    public boolean esMayorDe(int edad){
        return this.edad>edad;    
    }
    
    public String cuantoGana(){
        return nombre+" gana "+this.sueldo+"€";
    }
    
    public float proporcionSueldo(Persona per){
        return this.sueldo/per.sueldo;  
    }
    
    public float ajustaSueldoA(Persona per){
        if(this.puesto.equals(Constantes.ordeño)&&per.puesto.equals(Constantes.gerencia)){
           return this.sueldo=per.sueldo*0.5f;           
        }
        else if(this.puesto.equals(Constantes.ordeño)&&per.puesto.equals(Constantes.carniceria)){
           return this.sueldo=per.sueldo*1.25f;           
        }
        else if(this.puesto.equals(Constantes.carniceria)&&per.puesto.equals(Constantes.gerencia)){
           return this.sueldo=per.sueldo*0.25f;           
        }
        else if(this.puesto.equals(Constantes.carniceria)&&per.puesto.equals(Constantes.ordeño)){
           return this.sueldo=per.sueldo*0.75f;           
        }
        else if(this.puesto.equals(Constantes.gerencia)&&per.puesto.equals(Constantes.ordeño)){
           return this.sueldo=per.sueldo*1.5f;           
        }
        else if(this.puesto.equals("gerencia")&&per.puesto.equals("carniceria")){
          return  this.sueldo=per.sueldo*1.75f;           
        }else{
           return this.sueldo=per.sueldo;
        }    
    }
    
    public void quedarmeVacas(Vaca[]vac){
        this.vacas=new Vaca[vac.length];
        int contador=0;
        for(int i=0;i<vac.length;i++){
            if(this.puesto.equals(vac[i].funcion)){
             this.vacas[contador]=vac[i];
             contador++;
            }   
        }   
    }
    
    public void tengovacas(){
        System.out.print(this.nombre+" tiene estas vacas: ");
        for(int i=0;i<vacas.length;i++){
            if(this.vacas[i]==null){
                break;
            }else{
              System.out.print(this.vacas[i].nombre+" ");  
            }  
        }
    }
    
    //p1.misvacas[i].litros[j];
}


