
package arbolgenealogico;


public class ArbolGenealogico {
// Tarea: con esta clase en un main, crear tres generaciones de personas
//Abuela3 Abuelo3 Abuela2 Abuelo2 Abuela1 Abuelo1
//   |      |       |        |      |       |
//  ----------     -----------     -----------
//      |           |       |           |
//    Madre2     Padre2   Madre1      Padre1
//   |              |       |           |
//  ------------------    ------------------
//   |              |             |
// Hija2          Hijo2         Hijo1
//Construir este árbol genealógico usando la clase Persona
//y hacer sout(hijo1.devuelveMadre().devuelvePareja().devuelvePadre())
///Añadir a cada persona un padre y una madre
//Añadir a cada persona una pareja
//Añadir a cada persona un array de hijos
   
    public static void main(String[] args) {
        //Esto es un poco chapuza pero asi evitamos errores mas adelante.

        //Añadiendo datos a todos los abuelos, las parejas las añado fuera ya que de lo contrario me dan valores null, otra chapuza.
        Persona abuelo3=new Persona("Ramon","Vera","Escudo",false);
        Persona abuela3=new Persona("Lucia","Gonzalez","Ramirez",true);
        abuelo3.pareja=abuela3;
        abuela3.pareja=abuelo3;
        
        Persona abuelo2=new Persona("Paco","Diaz","Molina",false);
        Persona abuela2=new Persona("Dolores","Sanchez","Gopar",true);
        abuelo2.pareja=abuela2;
        abuela2.pareja=abuelo2;
        
        Persona abuelo1=new Persona("Gabril","Blanco","Velasco",false);
        Persona abuela1=new Persona("Clara","Alvarez","Ibañez",true);
        abuelo1.pareja=abuela1;
        abuela1.pareja=abuelo1;
        
        //Añadiendo datos a todos los padres, los apellidos los indicamos con los abuelos.
        Persona madre2=new Persona("Isabel", abuelo3.primerApellido, abuela3.primerApellido, true, abuelo3, abuela3);
        Persona padre2=new Persona("Antonio", abuelo2.primerApellido, abuela2.primerApellido, false, abuelo2, abuela2);
        madre2.pareja=padre2;
        padre2.pareja=madre2;
        
        Persona madre1=new Persona("Susana", abuelo2.primerApellido, abuela2.primerApellido, true, abuelo2, abuela2);
        Persona padre1=new Persona("Juanjo", abuelo1.primerApellido, abuela1.primerApellido, false, abuelo1, abuela1);
        madre1.pareja=padre1;
        padre1.pareja=madre1;
  
        //Añadiendo datos a todos los hijos, los apellidos los indicamos con los padres.
        Persona hija2=new Persona("Ana", padre2.primerApellido, madre2.primerApellido, true, padre2, madre2);
        Persona hijo2=new Persona("Ivan", padre2.primerApellido, madre2.primerApellido, false, padre2, madre2);
        Persona hijo1=new Persona("Pedro", padre1.primerApellido, madre1.primerApellido, false, padre1, madre1);
             
        //Añadiendo hijos
        Persona[]hijosDeAbuelos3={madre2};
        abuela3.hijos=abuelo3.hijos=hijosDeAbuelos3;
        
        Persona[]hijosDeAbuelos2={padre2,madre1};
        abuela2.hijos=abuelo2.hijos=hijosDeAbuelos2;
        
        Persona[]hijosDeAbuelos1={padre1};
        abuela1.hijos=abuelo1.hijos=hijosDeAbuelos1;
        
        Persona[]hijosDePadres2={hija2, hijo2};
        padre2.hijos=madre2.hijos=hijosDePadres2;
        
        Persona[]hijosDePadres1={hijo1};
        padre1.hijos=madre1.hijos=hijosDePadres1;
 
        //Pruebas, algunas no funcionan por el cambio de las funciones
        //Parejas:
        System.out.println(abuelo3.devuelvePareja());
        System.out.println(madre2.devuelvePareja());
        System.out.println(abuela2.devuelvePareja());
        System.out.println("----------------------\n");
        
        //Padres y Madres:
        System.out.println(madre2.devuelvePadre());
        System.out.println(padre1.devuelveMadre());
        System.out.println(hijo1.devuelveMadre());
        System.out.println("----------------------\n");
        
        //Hijos:
        System.out.println(abuelo3.devuelveHijos());
        System.out.println(madre2.devuelveHijos());
        System.out.println(abuelo2.devuelveHijos());
        System.out.println("----------------------\n");

        //
        hijo1.imprime();
        System.out.println("");
        hija2.imprime();
        System.out.println("");
        System.out.println(hijo1.devuelveMadre().devuelvePadre().devuelvePareja().nombre);
        System.out.println(hijo1.devuelveMadre().devuelvePadre().devuelvePareja().primerApellido);
        }
    }   

