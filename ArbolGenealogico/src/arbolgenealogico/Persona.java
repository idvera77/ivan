
package arbolgenealogico;

//Añadimos todas las variables para Persona.

import java.util.Scanner;

public class Persona {
         //Añadir a cada persona un padre y una madre
         //Añadir a cada persona una pareja
         //añadir a cada persona un array de hijos
        private String nombre;
        private String primerApellido;
        private String segundoApellido;
        private Persona madre;
        private Persona padre;
        private Persona pareja;
        private boolean sexo; //true femenino y false masculino
        private Persona[] hijos;
        
        //Contructor completo
        public Persona(String n,String pa,String sa,boolean s,Persona m,Persona pad,Persona par,Persona[] hijos){
            if(Persona.esNombreAceptable(n)){
                this.nombre=n;
            }else{
                this.nombre="";
            }
            this.primerApellido=pa;
            this.segundoApellido=sa;
            this.sexo=s;
            this.madre=m;
            this.padre=pad;
            this.pareja=par;
            this.hijos=hijos;
        }
        
        //Constructor sin ciertos atributos
        public Persona(String n,String pa,String sa,boolean s){
            if(Persona.esNombreAceptable(n)){
                this.nombre=n;
            }else{
                this.nombre="";
            }
            this.primerApellido=pa;
            this.segundoApellido=sa;
            this.sexo=s;
        }
        
        //Invalida nombres
        public static boolean esNombreAceptable(String nombre){
                if(nombre.equalsIgnoreCase("mamón")){
                        return false;
                }
                return true;
        }
        
        public Persona devuelveMadre(){
             return madre;
        }
        
        public Persona devuelvePadre(){
            return padre;
        }
        
        public Persona devuelvePareja(){
            return pareja;
        }
        
        public Persona[] devuelveHijos(){
                return hijos;
        }
        
        public int devuelveNHijos(){
            return hijos.length;
        }
        
        /**
         * Devuelve el hijo nº indice que tuvo esa persona. 
         * Es importante que nunca se ponga un índice mayor que el nº de hijos
         * @param indice
         * @return 
         */
        public Persona devuelveHijo(int indice){
                if(indice>hijos.length){
                    return null;
                }
                return this.hijos[indice];
        }
        
        public String imprimePersona(){
                String ret= this.nombre+" "+this.primerApellido+" "+this.segundoApellido+", ";
                if(this.sexo==false){ 
                    ret+="Hombre";
                }else{
                    ret+="Mujer";}
                return ret;
        }
        
        //Clave 1 de los getter: no tienen argumentos, a no ser que sea para coger un elemento de un array
        //Clave 2 : devuelven el mismo tipo del que sea la variable interna. Como nombre es string, devuelvo string.
        //Clave 3: Normalmente lo único que hacen es devolver el valor de la variable. Pero podría hacer otras cosas si lo estimamos oportuno.
        public String getNombre(){
                return this.nombre;
        }
        
        //Clave 1 de los setter: Devuelve void
        //Clave 2 : Recibe por parámetro un objeto del mismo tipo que la variable que queremos modificar
        //Clave 3: normalmente lo único que hace es establecer el valor, a no ser que queramos hacer comprobaciones de seguridad dentro
        public void setNombre(String n){
            if(Persona.esNombreAceptable(n)){
                this.nombre=n;
            }else{
                this.nombre="";
            }
        }
        
        public String getSexo(){
                if(this.sexo==false){ return "Hombre"; }
                else{ return "Mujer"; }
        }
        
        /**
         *  Establece el sexo de la persona. Espera recibir "Hombre" o "Mujer", si recibe cualquier otra cosa, lo cataloga como "Mujer"
         * @param nuevoS El String que contiene el nuevo sexo, que debe ser "Hombre" o "Mujer"
         */
        public void setSexo(String nuevoS){
                if(nuevoS.equalsIgnoreCase("hombre")){
                    this.sexo=false;
                }else{
                    this.sexo=true;
                }
        }
        
        public Persona[] getHijos(){
            return hijos;
        }
        
        public void setHijos(Persona[] h){
            hijos=h;
        }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Persona getMadre() {
        return madre;
    }

    public void setMadre(Persona madre) {
        this.madre = madre;
    }

    public Persona getPadre() {
        return padre;
    }

    public void setPadre(Persona padre) {
        this.padre = padre;
    }

    public Persona getPareja() {
        return pareja;
    }

    public void setPareja(Persona pareja) {
        this.pareja = pareja;
    }

        
}